/* 
 BackGround
 
 Handles the background
 */


class BackGround {
  //Declare the star array
  Star[] stars;

  //Constructor allows us to input how many stars should be on the screen at once
  public BackGround(int numStars) {

    //Initialize the array fill it with Star object instances
    stars = new Star[numStars];
    for (int i = 0; i< numStars; i++) {
      stars[i] = new Star();
    }
  }
    //update function will update the positions and render all stars in array

    void update() {
      for (int i = 0; i< stars.length; i++) {
        stars[i].update();
        stars[i].render();
      }
    }
  }
